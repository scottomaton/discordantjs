class Tile {
  constructor() {
    this.config = {
      image : undefined,
      passable : true
    };
  }

  image(newImage) {
    if (undefined !== newImage) {
      this.config.image = newImage;
      return this;
    }
    return this.config.image;
  }

  passable(newValue) {
    if (undefined !== newValue) {
      this.config.passable = newValue;
      return this;
    }
    return this.config.passable;
  }

  position(newPosition) {
    if (newPosition instanceof Vector3) {
      this.config.position = newPosition;
      return this;
    }
    return this.config.position;
  }
}

class Map {
  constructor(config = {}) {
    this.grid = new Grid({height : config.height || 3}); 
    this.tiles = [];
    this.transform = new Transform(); //todo - parent everything to this
    this.gameObject = new GameObject();

    this.test = this.grid.new_make_grid(3,3);

    this.tileset = {
      Grass : new Tile({image : './img/iso_tile_grass.gif'}),
      Dirt : new Tile({image : './img/iso_tile_dirt.gif'}),
      Water : new Tile({passable : false, image : './img/iso_tile_water_sm.gif'}),
      Concrete : new Tile({image : './img/iso_tile_concrete.gif'})
    };
    for (let y = 0; y < this.test.length; y++) {
      let row = [];
      for (let x = 0; x < this.test[y].length; ++x) {
        let newSprite = new Sprite();
        newSprite.transform().position(this.test[y][x]);
        newSprite.image('./img/iso_tile_dirt.gif');
        row.push(newSprite);
      }
      this.tiles.push(row);
    }
    /*this.tiles = this.test.map(square => {
      let newSprite = new Sprite();
      newSprite.transform().position(square);
      newSprite.image('./img/iso_tile_dirt.gif');
      return newSprite;
    });*/
  }

  at(x, y) {
    let gridAddress = this.grid.coordsToAddress(x, y);
    return this.tiles[gridAddress];
  }
}

class Grid {
  constructor(config) {
    this.gridHeight = config.height || 3;
    this._addresses = {};
    this._grid = this._makeGrid(this.gridHeight);
  }

  edges() {
    let edgeSquares = [];
  }

  at(x, y) {
    let actualAddress = this.coordsToAddress(x, y);
    return this._grid[actualAddress];
  }

  coordsToAddress(x, y) {
    let key = this.generateAddressKey(x,y);
    let address = this._addresses[ key ];
    return address;
  }

  generateAddressKey(x, y) {
    let key = `${x}:${y}`;
    return key;
  }

  _makeGrid(height) {
    let grid = [];
    let midPoint = Math.floor(height / 2.0);
    let y;
    let width = 0;
    let instance = this;
    for (y = 0; y <= height; ++y) {
      
      let yCoord = 64 * y;
      let xStart = -( width * 128 );
      for (let x = 0; x <= width; ++x) {
        let addressKey = `${x}:${y}`;
        let address = grid.length;
        instance._addresses[addressKey] = address;
        let xCoord = xStart + (x * 256);
        let zCoord = y - 100;

        let positionVector = new Vector3(xCoord, yCoord, zCoord);
        //row.push(positionVector); 
        grid.push(positionVector);
      }
      if (y < midPoint) {
        width++;
      } else {
        width--;
      }
      //grid.push(row);
    }

    
    return grid;
  }

  new_make_grid(height, width) {
    let table = [];
    let TILE_WIDTH = 128;
    let TILE_HEIGHT = 64;
    for (let y = 0; y < height; ++y) {
      let row = [];
      let xStart = -(TILE_WIDTH * y);
      let yStart = 64 * y;
      for (let x = 0; x < width; ++x) {
        let xPos = xStart + (x * TILE_WIDTH);
        let yPos = yStart + (x * TILE_HEIGHT);
        let zPos = yStart - 100;
        let positionVector = new Vector3(xPos, yPos, zPos);
        row.push(positionVector);
      }
      table.push(row);
    }

    //console.log( JSON.stringify(table, false, 4) );
    return table;
  }


  gridData() {
    return this._grid;
  }

  atPosition(positionAsVector2) {
    return this._grid[positionAsVector2.x()][positionAsVector2.y()];
  }
}

