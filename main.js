var runable = true;
var Process = {
  needsRepaint : true,
  halt : function() {
    runable = false;
  },
  isRunable : function() {
    return runable;
  }
};

function ObjectId() {
  return Math.floor( Math.random() * 100000000 );
}

const Time = {
  deltaTime : 0,
  time : performance.now()
};

class AudioClip {
  constructor(config = {}) {
    this.config = {
      source : config.source,
      loop : config.loop,
      autoplay : config.autoplay
    };
    if ('boolean' !== typeof this.config.loop) {
      this.config.loop = false;
    }
    if ('boolean' !== typeof this.config.autoplay) {
      this.config.autoplay = false;
    }
    if (this.config.source) {
      this.audioElement = new Audio(this.config.source);
    }
  }

  source(newSource) {
    if (undefined !== newSource) {
      this.config.source = newSource;
      this.audioElement = new Audio(this.config.source);
      return this;
    }
    return this.config.source;
  }

  autoplay(newSetting) {
    if (undefined !== newSetting) {
      this.config.autoplay = newSetting;
      return this;
    }
    return this.config.autoplay;
  }

  loop(newSetting) {
    if (undefined !== newSetting) {
      this.config.loop = newSetting;
      return this;
    }
    return this.config.loop;
  }
  play() {
    if (this.audioElement) {
      this.audioElement.play();
    }
  }
}

const BUTTON_STATE = {
  UP : 0,
  DOWN : 1
};

const Input = (function() {
  var inputManager = {
    gamePads : [],
    buttonPressed : buttonPressedFn,
    buttonHeld : buttonHeldFn,
    getAxis : getAxisFn,
    poll : pollInputDevices 
  };

  window.addEventListener('gamepadconnected', e => {
    let gp = navigator.getGamepads()[e.gamepad.index];
    inputManager.gamePads.push(new GamePad(gp));
    console.log('Gamepad registered');
  });

  function buttonPressedFn(buttonNumber, controllerIndex) {
    let isPressed = false;
    let gamePads = [];
    inputManager.gamePads.forEach(gp => {
      let instanceIsDown = gp.buttonPressed(buttonNumber);
      if (instanceIsDown) {
        isPressed = true;
        gamePads.push(gp);
      }
    });
    return isPressed;
  }
 
  function getAxisFn(axisNumber) {
    let value = 0;
    inputManager.gamePads.forEach(gp => {
      value = gp.axis(axisNumber);
    });
    return value;
  }

  function buttonHeldFn(buttonNumber, controllerIndex) {
    let isHeld = false;
    let gamePads = [];
    inputManager.gamePads.forEach(gp => {
      let instanceIsDown = gp.buttonHeld(buttonNumber);
      if (instanceIsDown) {
        isHeld = true;
        gamePads.push(gp);
      }
    });

    return isHeld;
  }

  function pollInputDevices() {
    this.gamePads.forEach(gp => {
      gp.poll();
    });
  }

  return inputManager;
})();

class GamePadButton {
  constructor() {
    this.currentState = BUTTON_STATE.UP;
    this.previousState = BUTTON_STATE.UP;
    this.heldDownDuration = 0;
  }

  poll() {
    var state = this.currentState + this.previousState;
    //state == 1 => button has changed position
    //state == 2 => button is held down
    //state == 0 => button has been up for two frames
    if (state <= 1) {
      this.heldDownDuration = 0;
    }
    if (state === 2) {
      this.heldDownDuration += Time.deltaTime;
    }
  }

  pressed() {
    return this.currentState === BUTTON_STATE.DOWN && this.heldDownDuration === 0;
  }

  held() {
    return this.currentState === BUTTON_STATE.DOWN && this.previousState === BUTTON_STATE.DOWN;
    //return this.currentState === BUTTON_STATE.DOWN && this.heldDownDuration > 0;
  }

  setState(newState) {
    this.previousState = this.currentState;
    this.currentState = newState;
  }

  getState() {
    return this.currentState;
  }
}

class InputAxis {
  constructor() {
    this.inputValue = 0;
  }

  setAxisValue(newValue) {
    this.inputValue = newValue;
  }

  getAxisValue() {
    return this.inputValue;
  }
}

class GamePad {
  constructor(gp) {
    if (!gp) {
      throw new Error('Unable to parse gamepad');
    }
    this.buttons = [];
    this.id = gp.id;
    this.index = gp.index;
    this.buttons = gp.buttons.map(button => {
      return new GamePadButton();
    });
    this.axes = gp.axes.map(axis => {
      return new InputAxis();
    });
  }
  
  buttonHeld(buttonNumber = 0) {
    return this.buttons[buttonNumber].held();
  }

  buttonPressed(buttonNumber = 0) {
    return this.buttons[buttonNumber].pressed();
  }

  axis(axisNumber) {
    //return this value of this axis
    return this.axes[axisNumber].getAxisValue();
  }

  poll() {
    //update the current values for buttons and axis
    let gp = navigator.getGamepads()[this.index];
    if (!gp) {
      return;
    }
    gp.buttons.forEach((rawButton, index) => {
      let state = BUTTON_STATE.UP;
      if (rawButton.pressed === true) {
        state = BUTTON_STATE.DOWN;
      }
      this.buttons[index].setState(state);
      this.buttons[index].poll();
    });

    gp.axes.forEach((rawAxis, index) => {
      this.axes[index].setAxisValue(rawAxis);
    });
  }
}

class Camera {
  constructor() {
    //let screenWidth = document.documentElement.clientWidth;
    //let screenHeight = document.documentElement.clientHeight;
    
    let screenWidth = window.innerWidth;
    let screenHeight = window.innerHeight;

    this.frontBuffer = new RenderBuffer({width: screenWidth, height : screenHeight});
    this.backBuffer = new RenderBuffer();
    this.config = {
      transform : new Transform(),
      width : screenWidth,
      height : screenHeight,
      backgroundColor : null 
    };

    document.getElementsByTagName('body')[0].appendChild( this.frontBuffer.rawCanvas() );
  }
  
  renderTarget() {
    return this.frontBuffer.ctx;
  }

  clear() {
    this.frontBuffer.clear();
  }

  setResolution(res) {
    this.frontBuffer.setResolution(res);
  }

  backgroundColor(newColor) {
    if (undefined !== newColor) {
      this.config.backgroundColor = newColor;
      return this;
    }
    return this.config.backgroundColor;
  }

  paint(scene) {
    if (scene instanceof Scene !== true) {
      return;
    }
    //Render background color, if exists
    if (this.config.backgroundColor) {
      this.frontBuffer.ctx.fillStyle = this.config.backgroundColor;
      this.frontBuffer.ctx.fillRect(0, 0, this.config.width, this.config.height);
    }
    let camera = this;
    scene.iterateGameObjects(function(gameObject) {
      //This currently ignores z value and will paint based on the order
      //in the array, not the actual distance to the camera
      //This also doesn't handle anything like camera position
      if (gameObject.active() === false) {
        return;
      }
      if (gameObject instanceof Sprite) {
        try {
          camera.drawSprite(gameObject);
        } catch(e) {
          throw e;
          Process.halt();
        }
      }
    });
  }

  transform() {
    return this.config.transform;
  }

  drawSprite(sprite) {
    let xOffset = this.transform().position().x();
    let yOffset = this.transform().position().y();
    let x = xOffset + sprite.transform().position().x(); 
    let y = yOffset + sprite.transform().position().y();
    let width = sprite.bounds().width() * sprite.transform().scale().x();
    let height = sprite.bounds().height() * sprite.transform().scale().y();
    let image = sprite.image().rawImage();
    let yRotation = sprite.transform().rotation().eulerAngles().y();
  
    let tX = sprite.transform().position().x()  + ( width * 0.5 );
    let tY = sprite.transform().position().y() + ( height * 0.5 );

    this.frontBuffer.ctx.save();
    this.frontBuffer.ctx.translate(tX, tY );
    this.frontBuffer.ctx.rotate(yRotation * Math.PI/180);
    this.frontBuffer.ctx.translate(0 - tX, 0 - tY);
    this.frontBuffer.ctx.drawImage(image, x, y, width, height);
    this.frontBuffer.ctx.restore();
  }

  /*
  drawImage(image, position, scale) {
    let xOffset = this.transform().position().x();
    let yOffset = this.transform().position().y();
    let width = image.width * scale.x();
    let height = image.height * scale.y();
    let yRotation = image.transform().rotation().y(); 
    this.frontBuffer.ctx.rotate(yRotation*Math.PI/180);

    this.frontBuffer.ctx.drawImage(image, position.x(), position.y(), width, height);
    //this.frontBuffer.drawImage(image, x, y);
  }*/
}

class RenderBuffer {
  constructor(config = {}) {
    this._canvas = document.createElement('canvas');
    this.ctx = this._canvas.getContext('2d');
    this.config = {
      height : config.height || 0,
      width : config.width || 0
    };

    //this._canvas.setAttribute('height', this.config.height );
    //this._canvas.setAttribute('width', this.config.width );
    this.height(this.config.height);
    this.width(this.config.width);
  }

  rawCanvas() {
    return this._canvas;
  }

  toImage() {
    return this._canvas.toDataURL();
  }

  clear() {
    this.ctx.clearRect(0, 0, this.width(), this.height());
  }

  height(newHeight) {
    if (undefined !== newHeight) {
      this.config.height = newHeight;
      this._canvas.style.height = newHeight + 'px';
      return this;
    }
    return this.config.height;
  }

  width(newWidth) {
    if (undefined !== newWidth) {
      this.config.width = newWidth;
      this._canvas.style.width = newWidth + 'px';
      return this;
    }
    return this.config.width;
  }

  setResolution(res = {}) {
    this._canvas.setAttribute('height',  res.height || 100);
    this._canvas.setAttribute('width', res.width || 100);
  }

  drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight) {
    this.ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
  }
}

class Vector2 {
  constructor(x = 0, y = 0) {
    this.config = {
      x : x,
      y : y
    };
  }

  add(vectorB) {
    if (vectorB instanceof Vector2 !== true) {
      throw new Error('Typecast error. Cannot cast ' + typeof vectorB + ' to Vector3');
    }
    let vectorA = this;
    let sum = new Vector2();
    sum.x( vectorA.x() + vectorB.x() );
    sum.y( vectorA.y() + vectorB.y() );
  }

  subtract(vectorB) {
    if (vectorB instanceof Vector2 !== true) {
      throw new Error('Typecast error. Cannot cast ' + typeof vectorB + ' to Vector3');
    }
    let vectorA = this;
    let sum = new Vector2();
    sum.x( vectorA.x() - vectorB.x() );
    sum.y( vectorA.y() - vectorB.y() );
  }

  x(newX) {
    if (undefined !== newX) {
      this.config.x = newX;
      return this;
    }
    return this.config.x;
  }

  y(newY) {
    if (undefined !== newY) {
      this.config.y = newY;
      return this;
    }
    return this.config.y;
  }

  toString() {
    return `[${this.config.x}, ${this.config.y}]`;
  }
}

class Vector3 {
  constructor(x = 0, y = 0, z = 0) {
    this.config = {
      x : x,
      y : y,
      z : z
    };
  }
  
  toString() {
    return `[${this.config.x}, ${this.config.y}, ${this.config.x}]`;
  }

  add(vectorB) {
    if (vectorB instanceof Vector3 !== true) {
      throw new Error('Typecast error. Cannot cast ' + typeof vectorB + ' to Vector3');
    }
    let vectorA = this;
    let sum = new Vector3();
    sum.x( vectorA.x() + vectorB.x() );
    sum.y( vectorA.y() + vectorB.y() );
    sum.z( vectorA.z() + vectorB.z() );
  }

  subtract(vectorB) {
    if (vectorB instanceof Vector3 !== true) {
      throw new Error('Typecast error. Cannot cast ' + typeof vectorB + ' to Vector3');
    }
    let vectorA = this;
    let sum = new Vector3();
    sum.x( vectorA.x() - vectorB.x() );
    sum.y( vectorA.y() - vectorB.y() );
    sum.z( vectorA.z() - vectorB.z() );
  }

  x(newX) {
    if (undefined !== newX) {
      this.config.x = newX;
      return this;
    }
    return this.config.x;
  }

  y(newY) {
    if (undefined !== newY) {
      this.config.y = newY;
      return this;
    }
    return this.config.y;
  }

  z(newZ) {
    if (undefined !== newZ) {
      this.config.z = newZ;
      return this;
    }
    return this.config.z;
  }
}

class Rect {
  constructor(x = 0, y = 0, height = 0, width = 0) {
    this.config = {
      x : x,
      y : y,
      height : height,
      width: width
    };
  }

  x(newX) {
    if (undefined !== newX) {
      this.config.x = newX;
      return this;
    }
    return this.config.x;
  }

  y(newY) {
    if (undefined !== newY) {
      this.config.y = newY;
      return this;
    }
    return this.config.y;
  }

  height(newHeight) {
    if (undefined !==  newHeight) {
      this.config.height = newHeight;
      return this;
    }
    return this.config.height;
  }

  width(newWidth) {
    if (undefined !==  newWidth) {
      this.config.width = newWidth;
      return this;
    }
    return this.config.width;
  }

  contains(point) {
    let maxX = this.x() + this.width();
    let maxY = this.y() + this.height();
    if (point.x < this.x() || point.x > maxX) {
      return false;
    }
    if (point.y < this.y() || point.y > maxY) {
      return false;
    }
    return true;
  }
}

class Rotation {
  constructor(eulerX = 0, eulerY = 0, eulerZ = 0) {
    this.config = {
      eulerX : eulerX,
      eulerY : eulerY,
      eulerZ : eulerZ
    };
  }

  eulerAngles() {
    return new Vector3(this.config.eulerX, this.config.eulerY, this.config.eulerZ);
  }
}



class GameObject {
  constructor() {
    this.id = ObjectId();
    this.config = {
      active : true,
      transform : new Transform(),
      components : [],
      parentObject : null,
      children : []
    };
  }

  transform() {
    return this.config.transform;
  }

  active(setActive) {
    if ('boolean' === typeof setActive) {
      this.config.active = setActive;
      return this;
    }
    return this.config.active;
  }

  parent(newParent) {
    if (newParent instanceof GameObject) {
      if (this.config.parentObject instanceof GameObject) {
        this.config.parerentObject.removeChild(this);
      }
      this.config.parentObject = newParent;
      newParent.addChild(this);
      return this;
    }
    return this.config.parentObject;
  }

  addChild(newChild) {
    if (newChild instanceof GameObject !== true) {
      return this;
    }
    this.config.children.push(newChild);
    return this;
  }

  removeChild(childToRemove) {
    if (!childToRemove || !childToRemove.id) {
      return this;
    }
    let removedChild = null;
    for (let k = 0; k < this.config.children.length; ++k) {
      if (this.config.children[k].id === childToRemove.id) {
        removedChild = this.config.children.splice(k,1)[0];
        break;
      }
    }
    return removedChild;
  }

  children() {
    return this.config.children;
  }
  
  addComponent(component) {
    this.config.components.push(component);
  }
  
  update() {
    let k = 0;
    for (k; k < this.config.components.length; ++k) {
      try {
        this.config.components[k].update.call(this);
      } catch(e) {
        throw e;
        Process.halt();
        //ignore
      }
    }
  }
  paint() {

  }
}

class GUIText extends GameObject {
  constructor(config = {}) {
    super();
    this.config = this.config || {};
    
    let defaults = {
      active : true,
      position : new Vector3(0,0,0),
      text : '',
      font : '12px Sans-Serif',
      fillColor : '#000000',
      strokeColor : undefined
    };
    Object.assign(this.config, defaults);
  }

  text(newText) {
    if (undefined !== newText) {
      this.config.text = newText;
      return this;
    }
    return this.config.text;
  }

  font(newFont) {
    if (undefined !== newFont) {
      this.config.font = newFont;
      return this;
    }
    return this.config.font;
  }

  position(newPosition) {
    if (undefined !== newPosition) {
      this.config.position = newPosition;
      return this;
    }
    return this.config.position;
  }
}

class GUI extends GameObject {
  constructor(config = {}) {
    super(config);
    this.config = {
      camera : undefined,
      elements : []    
    };
  }

  camera(newCam) {
    if (undefined !== newCam) {
      this.config.camera = newCam;
      return this;
    }
    return this.config.camera;
  }

  addElement(newElement) {
    this.config.elements.push(newElement); 
  }

  render() {
    let cam = this.camera();
    if (!cam) {
      return;
    }
    let k = 0;
    let len = this.config.elements.length;
    let elems = this.config.elements.sort(SortGUIElements);
    let ctx = cam.renderTarget();
    for (k; k < len; ++k) {
      if (elems[k] instanceof GUIText) {
        this._renderText(elems[k], ctx);
      }
    }
  }

  _renderText(textElem, ctx) {
    let text = textElem.text();
    let font = textElem.font();
    let x = textElem.position().x();
    let y = textElem.position().y();
    ctx.font = font;
    ctx.fillStyle = '#FFFFFF';
    ctx.fillText(text, x, y); 
  }
}

function SortGUIElements(a, b) {
  if ('function' !== typeof a.position || 'function' !== typeof b.position) {
    return 0;
  }
  let zA = a.position().z();
  let zB = b.position().z();

  if (zA > zB) {
    return 1;
  } else if (zA < zB) {
    return -1;
  } else {
    return 0;
  }
}

class Transform {
  constructor() {
    this.config = {
      scale    : new Vector3(1,1,1),
      position : new Vector3(),
      rotation : new Rotation(),
    };
  }

  translate(amount, y, z) {
    if (amount instanceof Vector2) {
      this.translate2D(amount);
    } else if (amount instanceof Vector3) {
      this.translate3D(amount);
    } else if ('number' === typeof amount) {
      this.translate3D(new Vector3(amount, y, z));
    } else {
      return this;
    }
    /** @todo check to see if we actually moved at all **/
    Process.needsRepaint = true;
    return this;
  }

  translate2D(vector) {
    this.position().x( this.position().x() + vector.x() );
    this.position().y( this.position().y() + vector.y() );
    return this;
  }

  translate3D(vector) {
    this.position().z( this.position().z() + vector.z() ); 
    this.position().x( this.position().x() + vector.x() );
    this.position().y( this.position().y() + vector.y() );
    return this;
  }

  position(newPosition) {
    if (undefined !== newPosition) {
      this.config.position = newPosition;
      return this;
    }
    return this.config.position;
  }

  scale(newScale) {
    if (newScale instanceof Vector3) {
      this.config.scale = newScale;
    } else if (newScale instanceof Vector2) {
      this.config.scale = new Vector3(newscale.x(), newscale.y(), 1);
    } else if ('number' === typeof newScale) {
      this.config.scale = new Vector3(newScale, newScale, newScale);
    } else {
      return this.config.scale;
    }
    Process.needsRepaint = true;
    return this;
  }

  rotation(rotation) {
    if (rotation instanceof Vector3) {
      this.config.rotation = new Rotation(rotation.x(), rotation.y(), rotation.z() );
      Process.needsRepaint = true;
    } else if (rotation instanceof Rotation) {
      Process.needsRepaint = true;
      this.config.rotation = rotation;
    } else {
      return this.config.rotation;
    }
  }

  rotate(amount) {
    Process.needsRepaint = true;
    let rotationVector = new Vector3(0,0,0);
    if (amount instanceof Vector2) {
      rotationVector.x(amount.x()).y(amount.y()).z(0);
    } else if (amount instanceof Vector3) {
      rotationVector = amount;
    } else if ('number' === typeof amount) {
      rotationVector.x(amount).y(amount).z(amount);
    }

    let resolvedRotation = new Vector3(0,0,0);
    let currentRotation = this.rotation().eulerAngles();
    resolvedRotation.x( currentRotation.x() + rotationVector.x() );
    resolvedRotation.y( currentRotation.y() + rotationVector.y() );
    resolvedRotation.z( currentRotation.z() + rotationVector.z() );

    this.rotation( resolvedRotation );
    return this;
  }
}

class ImageSource {
  constructor() {
    this.config = {
      bounds : new Rect(),
      ready : false,
      rawImage : new Image()
    };
  }

  source(newSource) {
    if (undefined !== newSource) {
      let obj = this;
      this.config.ready = false;
      this.config.rawImage.src = newSource;
      this.config.rawImage.onload = function onLoad(event) {
        /** @set bounds **/
        obj.config.bounds = new Rect()
          .x(0)
          .y(0)
          .width(this.width)
          .height(this.height);
        obj.config.ready = true; 
      };
      return this;
    } else {
      return this.config.rawImage.src;
    }
  }

  rawImage() {
    return this.config.rawImage;
  }

  bounds() {
    return this.config.bounds;
  }
}

class Sprite extends GameObject {
  constructor() {
    super();
//    this.config.transform = new Transform();
    this.config.img = new ImageSource();
    
    this._frameRate = 24;
    this._accumulator;
    this.frames = [];
    this.currentFrameIndex = 0;
  }

  bounds() {
    return this.frames[ this.currentFrameIndex].bounds();
  }

  image(newSrc) {
    if (undefined !== newSrc) {
      Process.needsRepaint = true;
      let newImageSource = new ImageSource();
      newImageSource.source(newSrc);
      this.frames.push(newImageSource);
     // this.config.img.source(newSrc); 
      return this;
    }
    return this.frames[ this.currentFrameIndex ];
    //return this.config.img;
  }

  addAnimationFrame(newSprite) {
    if (newSprite instanceof Sprite) {
      Process.needsRepaint = true;
      this.frames.push(newSprite);
    }
    return this;
  }

  //transform() {
  //  return this.config.transform;
  //}

  /*
  paint(camera) {
    let x = this.transform().position().x();
    let y = this.transform().position().y();
    let cameraX = camera.transform().position().x();
    let cameraY = camera.transform().position().y();
    let position = new Vector3(cameraX + x, cameraY + y, this.transform().position().z());
    camera.drawImage( this, position, this.transform().scale());
  }
  */
  update() {
    super.update();
    this._accumulator += Time.deltaTime;
    if (this._accumulator < this._frameRate) {
      return;
    }
    this._accumulator = 0;
    this.currentFrameIndex++;
    if (this.currentFrameIndex >= this.frames.length) {
      this.currentFrameIndex = 0;
    }
  } 
}

function traverse(node) {
  node.children().filter(child => {
    return child.active() === true;
  }).forEach(child => {
    
  });
}

class Scene {
  constructor(config = {}) {
    this.name = config.name || 'Untitled Scene';
    this.gameObjects = {};
    this._objectMapHash = {};
    this._gameObjectCount = 0;
    this.sceneGraph = [];

    this.root = new GameObject();
  }

  clear() {
    this.sceneGraph = [];
  }

  /**
   * Trying a new technique of using a single root node
   * and traversing from there instead of an array
   */
  render() {
    let node = this.root;
    traverse(node);
  }

  addGameObject(gameObject) {
    Process.needsRepaint = true;
    if (gameObject.id === undefined) {
      gameObject.id = ObjectId();
    }
    this.sceneGraph.push(gameObject);
  }

  iterateGameObjects(callback) {
    return this.sceneGraph.sort((a, b) => {
      if ('function' !== typeof a.transform || 'function' !== typeof b.transform) {
        return 0;
      }
      let aZ = a.transform().position().z();
      let bZ = b.transform().position().z();
      if (aZ < bZ) {
        return -1;
      } else if (aZ > bZ) {
        return 1;
      } else {
        return 0;
      }
    }).forEach(gameObject => {
      return callback(gameObject);
    });
  }

  old_addGameObject(gameObject) {
    let sceneObject;
    if (gameObject instanceof GameObject !== true) {
      sceneObject = new GameObject(gameObject);
    } else {
      sceneObject = gameObject;
    }
    if (!sceneObject.id) {
      sceneObject.id = ObjectId(); 
    }
    this.gameObjects[ sceneObject.id ] = sceneObject; 
    this._objectMapHash[ this._gameObjectCount ] = sceneObject.id;
    this._gameObjectCount++;
    return sceneObject.id;
  }

  getObject(objectId) {
    return this.gameObjects[objectId];
  }


  update() {
    this.iterateGameObjects(gameObject => {
      try {
        gameObject.update.call(gameObject);
      } catch(e) {
        throw e;
        Process.halt();
      }
    });
  }

  old_iterateGameObjects(iterator) {
    let k = 0;
    for (k; k < this._gameObjectCount; ++k) {
      let key = this._objectMapHash[ k ];
      let gameObjectInstance = this.gameObjects[ key ];
      try {
        iterator(gameObjectInstance);
      } catch(err) {
        throw err;
        Process.halt();
      }
    }

  }
   
  nextTick() {
    //this.camera.clear();
    this.update();
  }
  

}
/*
class InputController {
  constructor() {

  }

  poll() {

  }
}

class KeyboardController extends InputController {
  constructor() {
    super();
  }

  poll() {
        
  }
}*/

const RUN_STATE = {
  STOP : 0,
  RUN : 1
};

class Game {
  constructor() {
    //this.input = new InputController();
    this.currentScene = new Scene(); 
    this.runState = RUN_STATE.STOP;
    this.camera = new Camera();
    this.gamePads = [];
    this.GUI = new GUI();
    this.GUI.camera(this.camera);
    this.stats = {
      fps: 0
    };

  }

  addGamePad(gp) {
    this.gamePads.push(gp);
  }

  run() {
    this.runState = 1;
    let instance = this;
    requestAnimationFrame(function(timestamp) {
      instance.nextTick(instance); 
    });
  }

  pause() {
    this.runState = RUN_STATE.STOP;
  }

  resume() {
    this.runState = RUN_STATE.RUN;
  }

  nextTick(instance) {
    if (Process.isRunable() !== true) {
      return;
    }
    requestAnimationFrame(function(timeStamp) {
      let frametime = 0.06; //frames per millisecond
      if (timeStamp - Time.time < frametime) {
        console.log('skip');
        return instance.nextTick(instance);
      }
      
      //let dt = timeStamp - Time.time; //this is total elapsed time since last frame, in milliseconds
      //let deltaTime = (timeStamp - Time.time) * frametime;
      //Time.deltaTime = deltaTime;
      Time.deltaTime = (timeStamp - Time.time) / 1000;
      Time.time = performance.now();
      Input.poll();
      instance.camera.clear();
      if (instance.runState === RUN_STATE.RUN) {

        instance.currentScene.nextTick();
      }
      if (Process.needsRepaint) {
        instance.camera.paint(instance.currentScene);
      } else {
        console.log('idle');
      }
      instance.GUI.render();
      instance.nextTick(instance);
    });
  }
}


