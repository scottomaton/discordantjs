(function(exports) {
  class KeyState {
    constructor() {
      this.isPressed = false;
      this.isHeld = false;
      this.heldDuration = 0.0;
      this.isReleased = false;
    }
  }

  class KeyboardInput {
    constructor() {
      this.keys = {};
      
      let instance = this;

      window.onkeydown = function(event) {
        instance.keys[ event.key ] = instance.keys[ event.key ] || new KeyState();
        let currentState = instance.keys[ event.key ];

        if (currentState.isHeld) {
          instance.heldDuration += Time.deltaTime;
        } else if (currentState.isPressed) {
          currentState.isHeld = true;
        } else {
          currentState.isPressed = true;
        }
      };

      window.onkeyup = function(event) {
        instance.keys[ event.key ] = instance.keys[ event.key ] || new KeyState();
        
        instance.keys[ event.key ].isPressed = false;
        instance.keys[ event.key ].isHeld = false;
        instance.keys[ event.key ].heldDuration = 0.0;
        instance.keys[ event.key ].isReleased = true;
      };
    }

    poll() {
      //sweep keys and mark any released as no longer released
      for (let i in this.keys) {
        this.keys[i].isReleased = false;
      }
    }

    keyDown(key) {
      let isDown = undefined !== this.keys[ key ] && this.keys[ key ].isDown === true;
      return isDown;
    }

    keyHeld(key) {
      let isHeld = undefined !== this.keys[ key ] && this.keys[ key ].isHeld === true;
      return isHeld;
    }

    keyHeldForDuration(key, duration) {
      let isHeld = this.keyHeld(key);
      return (isHeld === true && this.heldDuration >= duration);
    }

    keyReleased(key) {
      let isReleased = undefined !== this.keys[ key ] && this.keys[ key ].isReleased === true; 
      return isReleased;
    }
  }

  exports.Keyboard = new KeyboardInput();

}(exports));
